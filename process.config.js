module.exports = {
  apps: [{
    name: 'server',
    script: 'dist/fake-http-server.js',
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '1G',
  },
  {
    name: 'server',
    script: 'dist/fake-http-client.js',
    instances: process.env.NUM_OF_CLIENTS || 2,
    autorestart: true,
    watch: true,
    max_memory_restart: '1G',
  }],
};
