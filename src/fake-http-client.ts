import request from 'request';
import { promisify } from 'util';
import { makeRandomPath, makeRandomPayload } from './generate-mock-db';
const wait = promisify(setTimeout);

const methods = ['GET', 'POST', 'PUT', 'DELETE'];

function makeRequest() {
    return new Promise((resolve, reject) => {
        const method = methods[Math.floor(Math.random() * methods.length)];
        request({
            uri: `http://${process.env.MOCK_CLIENT_SERVER || 'localhost:8080'}${makeRandomPath()}`,
            method,
            ...['POST', 'PUT'].includes(method) ? {
                json:true,
                body: makeRandomPayload(),
            } : {},
        }, (err, res, b) => {
            if (err) {
                return reject(err);
            }
            console.log(res.statusCode, typeof b == 'string' ? b : JSON.stringify(b));
            resolve(b);
        })
    });
}

(async () => {
    while (true) {
        try {
            await makeRequest();
            await wait(1000);
        } catch (err) {
            console.log(err.message);
            await wait(500);
            continue;
        }
    }
})();