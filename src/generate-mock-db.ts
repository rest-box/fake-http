import * as faker from 'faker'
import { sample, forEach, pickBy } from 'lodash';
const shortButMakeSense = [ faker.database, faker.internet, faker.name, faker.system ];


function makeRandomPayload() {
    const res: any = {};
    const [ta, tb] = [sample(shortButMakeSense), sample(shortButMakeSense)];
    
    forEach(ta, (value, key) => {
        if('function' === typeof value) {
            res[key] = (<any>value)();
        }
    });

    forEach(tb, (value, key) => {
        if('function' === typeof value) {
            res[key] = (<any>value)();
        }
    });

    return pickBy(res, () => Math.random() > Math.random())
}

function makeRandomPath() {
    if(Math.ceil(Math.random() * 100) === 95) {
        return `/`;
    }
    const domain = faker.internet.domainWord()
    if(domain.charCodeAt(0) % 2 === 0) {
        return `/${domain}`;
    }
    return `/${domain}/${faker.internet.color().replace('#', '')}`
}

export {
    makeRandomPayload,
    makeRandomPath,
}