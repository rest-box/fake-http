import express from 'express';
import { sample } from 'lodash';
import { makeRandomPayload } from './generate-mock-db';

const morgan = require('morgan');

const app = express();

app.use(morgan('dev'));

const noneSuccessCodes = [
    400,
    403,
    406,
    412,
    500,
    503,
    506,
    504,
    401,
    404,
    502,
];

app.all('*', (_req, res) => {
    // 20% to get error
    if(Math.ceil(Math.random() * 10) > 8) {
        const resCode = sample(noneSuccessCodes);
        res.status(resCode || 400)
        res.json({error: `some random error with code ${resCode || 400}`});
        return;
    }
    res.json(makeRandomPayload());
});

app.listen(process.env.MOCK_SERVER_PORT || '8081');